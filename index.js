// TODO
const express = require('express');
const app = express();

const PORT = 56201;
if ('PORT' in process.env) {
    const PORT = process.env.PORT;
}
const bodyParser = require('body-parser');
app.use(bodyParser.text());

app.post('/square', (req,res) => {
    const num = +req.body;
    res.json({
        number: num,
        square: num*num
    });
});

app.post('/reverse', (req,res) => {
    const text = req.body;
    res.setHeader('content-type', 'text/plain');
    res.send(text.split("").reverse().join(""));
});

const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

app.get('/date/:year/:month/:day', (req,res) => {
    let baseDate = new Date(Date.UTC(req.params.year, (+req.params.month) - 1, req.params.day));
    let testDate = new Date();
    let currentDate = Date.UTC( testDate.getFullYear(), testDate.getMonth(), testDate.getDate());
    let timeDiff = Math.abs(currentDate - baseDate);
    res.json({
        "weekDay": days[baseDate.getDay()],
        "isLeapYear": leapYear(req.params.year),
        "difference": Math.floor(timeDiff / (1000 * 3600 * 24))
    });
});

function leapYear(year)
{
    return ((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0);
}

app.listen(PORT,()=>{
    console.log('App running on port '+PORT);
});

